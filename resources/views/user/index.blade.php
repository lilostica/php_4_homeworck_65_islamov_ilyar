@extends('layouts.app')
@section('content')
    <h1 class="main-title">{{$user->name}}</h1>
    <button type="button" class="btn btn-primary main-title"><a class="links" href="{{route('photo.create')}}">Add Photo</a></button>

    <div class="row">
        @foreach($user->photos as $photo)
        <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                @if($photo->photo !== 'noPhoto.jpg')
                    <img class="card-img-top" src="{{asset('/storage/' .
                        $photo->photo)}}" alt="photo">
                @else
                    <img src="{{asset('noPhoto.jpg')}}" class="card-img-top" alt="No photo">
                @endif
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#comment{{$photo->id}}" aria-expanded="false" aria-controls="collapseExample">
                            Comments
                        </button>
                        @foreach($photo->comments->where('active',true) as $comment)
                            <div class="collapse comment" id="comment{{$photo->id}}">
                                <div class="card card-body">
                                    <h4>{{$comment->user->name}}</h4>
                                    {{$comment->body}}
                                </div>
                                @can('destroy',$comment)
                                    <form  method="post"  action="{{ route('destroy-comment',$comment->id) }}">
                                        @method('DELETE')
                                        @csrf
                                        <button class="like btn-danger" type="submit">Delete</button>
                                    </form>
                                @endcan
                            </div>
                        @endforeach
                <p class="photo-title">{{$photo->title}}</p>
                <div class="card-body">
                    <p class="card-text">{{$photo->description}}</p>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <form method="post" action="{{route('photo.destroy',['photo' => $photo])}}">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-sm btn-outline-secondary">delete</button>
                            </form>
                            <button type="button" class="btn btn-sm btn-outline-secondary"><a href="{{route('photo.edit',['photo' => $photo])}}">edit</a></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            @endforeach
    </div>
    @endsection

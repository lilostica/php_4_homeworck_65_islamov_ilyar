@extends('layouts.app')

@section('content')
<h1 class="main-title">Your tape</h1>
@foreach($user->subscribersOf as $user)
    @foreach($user->photos as $photo )
<div class="home-tape">
<div class="card main-title" style="width: 18rem;">
    <h5 class="card-title title-card">{{$user->name}}</h5>
    @if($photo->photo !== 'noPhoto.jpg')
        <img class="card-img-top" src="{{asset('/storage/' .
                        $photo->photo)}}" alt="Photo">
    @else
        <img src="{{asset('noPhoto.jpg')}}" class="card-img-top" alt="No photo">
    @endif
    <div class="card-body">
        <p class="card-text">{{$photo->description}}</p>
        <form method="post" action="{{ route('like-action',$photo->id) }}">
            @csrf
            <button class="btn btn-primary like" type="submit">Like {{$photo->likes->count()}}</button>
        </form>
        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#comment{{$photo->id}}" aria-expanded="false" aria-controls="collapseExample">
           Comments
        </button>
        @foreach($photo->comments->where('active',true) as $comment)
            <div class="collapse comment" id="comment{{$photo->id}}">
                <div class="card card-body">
                    <h4>{{$comment->user->name}}</h4>
                    {{$comment->body}}
                </div>
                @can('destroy',$comment)
                <form  method="post"  action="{{ route('destroy-comment',$comment->id) }}">
                    @method('DELETE')
                    @csrf
                    <button class="like btn-danger" type="submit">Delete</button>
                </form>
                    @endcan
            </div>
            @endforeach
        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#add-comment{{$photo->id}}" aria-expanded="false" aria-controls="collapseExample">
            Add comment
        </button>
            <div class="collapse comment" id="add-comment{{$photo->id}}">
                <div class="card card-body">
                    <form method="post" action="{{route('add-comment',$photo->id)}}">
                        @csrf
                    <div class="form-group">
                        <label for="comment">Enter comment</label>
                        <textarea name="body" class="form-control @error('body') is-invalid @enderror" id="comment" rows="3"></textarea>
                        @error('body')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                        <button class="btn-primary" type="submit">Add comment</button>
                    </form>
                </div>
            </div>
    </div>
</div>
</div>
        @endforeach
    @endforeach
<h4 class="main-title">Recommended Users</h4>
<div class="row">
    @foreach($recommended_users as $user)
    <div class="col col-md-4">
                    <div class="card" style="width: 18rem; margin-top: 50px;">
                        <div class="card-body">
                            <h5 class="card-title">{{$user->name}}</h5>
                            <form method="post" action="{{route('subscribe',$user->id)}}">
                                @csrf
                                <button class="btn btn-primary" type="submit">Follow</button>
                            </form>
                        </div>
                    </div>
    </div>
@endforeach
</div>
@endsection

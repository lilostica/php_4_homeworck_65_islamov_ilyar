@extends('layouts.app')
@section('content')
    <h1 style="margin-top: 25px;">Add photo</h1>
    <form method="post" action="{{route('photo.update',$photo)}}" enctype="multipart/form-data">
        @method('put')
        @csrf
        <div class="form-group">
            <label for="title">Photo title</label>
            <input type="text"  value="{{$photo->title}}" class="form-control @error('title') is-invalid @enderror" id="title" name="title">
            @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <label for="description">Photo description</label>
        <div class="form-group">
            <textarea  name="description" id="description" cols="135" rows="5">{{$photo->description}}</textarea>
        </div>
        <figure class="figure">
        @if($photo->photo !== 'noPhoto.jpg')
            <img class="figure-img img-fluid rounded" src="{{asset('/storage/' .
                        $photo->photo)}}" alt="photo">
        @else
            <img src="{{asset('noPhoto.jpg')}}" class="figure-img img-fluid rounded" alt="No photo">
        @endif
        </figure>
        <div class="form-group">
            <div class="custom-file">
                <input type="file" class="custom-file-input @error('photo') is-invalid @enderror" id="customFile" name="photo">
                @error('photo')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <label class="custom-file-label" for="customFile">Choose picture</label>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Update photo</button>
    </form>
@endsection

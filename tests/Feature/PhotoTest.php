<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PhotoTest extends TestCase
{
    use RefreshDatabase;


    /**
     * @photo
     */
    public function testAddPhoto()
    {
        $user = factory(User::class)->create();
        $photo = [
            'user_id' => $user->id,
            'title' => 'test',
            'photo' => 'test.jpg',
            'description' => 'test'
        ];
        $this->actingAs($user);
        $response = $this->post(route('photo.store',$photo));
        $response->assertStatus(302);
    }

    /**
     * @photo
     */
    public function testCreatePhoto()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $response = $this->get(route('photo.create'));
        $response->assertStatus(200);
    }

    /**
     * @photo
     */
    public function testUnLoginCreatePhoto()
    {
        $response = $this->get(route('photo.create'));
        $response->assertStatus(302);
    }

    public function testUnLoginEditPhoto()
    {
        $response = $this->get("photo/1}/edit");
        $response->assertStatus(302);
    }
}

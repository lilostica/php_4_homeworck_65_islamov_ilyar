<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::resource('photo','PhotosController')->except(['index','show',]);
Auth::routes();
Route::get('/user_cabinet','UserController@index')->name('user_cabinet');
Route::post('photo/{id}/like','LikesController@addOrDeleteLike')->name('like-action');
Route::post('/user/{id}/subscribeOf','UserController@subscribe')->name('subscribe');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/add/comment/photo/{id}','CommentController@addComment')->name('add-comment');
Route::delete('/destroy/comment/{id}','CommentController@destroy')->name('destroy-comment');

<?php

namespace App\Orchid\Layouts;

use App\Comment;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CommentListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'comments';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('id','Author email')
                ->sort()
                ->filter(TD::FILTER_TEXT)
                ->render(function (Comment $comment){
                    return Link::make($comment->user->email)
                        ->route('platform.comment.edit',$comment);
                }),
            TD::set('created_at','Created')->sort(),
            TD::set('updated_at', 'Last_update')->sort()
        ];
    }
}

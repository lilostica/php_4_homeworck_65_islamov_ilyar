<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\CommentRequest;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * CommentController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @param CommentRequest $request
     * @param $photo_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addComment(CommentRequest $request, $photo_id)
    {
        $user = $request->user();
        $comment = new Comment();
        $comment->user_id = $user->id;
        $comment->photo_id = $photo_id;
        $comment->body = $request->input('body');
        $comment->active = false;
        $comment->save();
        return back()->with('status', 'Comment has added Wait for administrator confirmation');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);
        $this->authorize('destroy',$comment);
        $comment->delete();
        return back()->with('status', "{$comment->user->name} comment has deleted");
    }
}

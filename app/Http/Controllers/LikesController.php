<?php

namespace App\Http\Controllers;

use App\Like;
use App\Photo;
use Illuminate\Http\Request;

class LikesController extends Controller
{
    public function addOrDeleteLike(Request $request,$id)
    {
        $user = $request->user();
        $photo = Photo::findOrFail($id);
        $like = $photo->likes->where('user_id',$user->id);
        $array =  $like->toArray();
        $like_key = null;
        foreach ($array as $key)
        {
            $like_key = $key['id'];
        }
        if ($like->count() > 0)
        {
            $remove_like = Like::find($like_key);
            $remove_like->delete();
            return back()->with('status', 'like has removed!');
        }else
            {
                $like = new Like();
                $like->user_id = $user->id;
                $like->photo_id = $photo->id;
                $like->save();
                return back()->with('status',"Like has added to photo {$photo->name}");
            }
    }
}

<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $user = $request->user();
        return view('user.index',compact('user'));
    }


    public function subscribe(Request $request,$id)
    {
        $auth_user = $request->user();
        $fallow_user = User::findOrFail($id);
        if ($auth_user->subscribersOf->contains($fallow_user->id))
        {
            return back()->with('status', "You are already subscribed {$fallow_user->name}");
        }else
            {
                $auth_user->subscribersOf()->attach($fallow_user->id);
                return back()->with('status', "You subscribed {$fallow_user->name}");
            }
    }
}

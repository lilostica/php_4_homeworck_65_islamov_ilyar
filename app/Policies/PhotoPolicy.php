<?php

namespace App\Policies;

use App\Photo;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class PhotoPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function edit(User $user,Photo $photo)
    {
        return $user->id == $photo->user_id;
    }

    public function delete(User $user,Photo $photo)
    {
        return $user->id == $photo->user_id;
    }
}

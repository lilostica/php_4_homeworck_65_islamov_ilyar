<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubscribersTableSedder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subscribers')->insert(
            [
              'user_id' => 1,
              'follower_id' => 2
            ]
        );
        DB::table('subscribers')->insert(
            [
                'user_id' => 1,
                'follower_id' => 3
            ]
        );
        DB::table('subscribers')->insert(
            [
                'user_id' => 2,
                'follower_id' => 3
            ]
        );
        DB::table('subscribers')->insert(
            [
                'user_id' => 2,
                'follower_id' => 4
            ]
        );
        DB::table('subscribers')->insert(
            [
                'user_id' => 3,
                'follower_id' => 4
            ]
        );
        DB::table('subscribers')->insert(
            [
                'user_id' => 3,
                'follower_id' => 1
            ]
        );
        DB::table('subscribers')->insert(
            [
                'user_id' => 4,
                'follower_id' => 5
            ]
        );
        DB::table('subscribers')->insert(
            [
                'user_id' => 4,
                'follower_id' => 2
            ]
        );
        DB::table('subscribers')->insert(
            [
                'user_id' => 5,
                'follower_id' => 2
            ]
        );
        DB::table('subscribers')->insert(
            [
                'user_id' => 5,
                'follower_id' => 1
            ]
        );
        DB::table('subscribers')->insert(
            [
                'user_id' => 6,
                'follower_id' => 5
            ]
        );
        DB::table('subscribers')->insert(
            [
                'user_id' => 6,
                'follower_id' => 7
            ]
        );
        DB::table('subscribers')->insert(
            [
                'user_id' => 7,
                'follower_id' => 10
            ]
        );
        DB::table('subscribers')->insert(
            [
                'user_id' => 7,
                'follower_id' => 9
            ]
        );
        DB::table('subscribers')->insert(
            [
                'user_id' => 8,
                'follower_id' => 1
            ]
        );
        DB::table('subscribers')->insert(
            [
                'user_id' => 8,
                'follower_id' => 3
            ]
        );
        DB::table('subscribers')->insert(
            [
                'user_id' => 9,
                'follower_id' => 3
            ]
        );
        DB::table('subscribers')->insert(
            [
                'user_id' => 9,
                'follower_id' => 6
            ]
        );
        DB::table('subscribers')->insert(
            [
                'user_id' => 10,
                'follower_id' => 2
            ]
        );
        DB::table('subscribers')->insert(
            [
                'user_id' => 10,
                'follower_id' => 3
            ]
        );

    }
}
